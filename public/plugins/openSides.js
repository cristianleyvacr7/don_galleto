
function isScrolledIntoView(elem)
{
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();

		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();

		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function Utils() {

}

Utils.prototype = {
		constructor: Utils,
		isElementInView: function (element, fullyInView) {
				var pageTop = $(window).scrollTop();
				var pageBottom = pageTop + $(window).height();
				var elementTop = $(element).offset().top;
				var elementBottom = elementTop + $(element).height();

				if (fullyInView === true) {
						return ((pageTop < elementTop) && (pageBottom > elementBottom));
				} else {
						return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
				}
		}
};

var Utils = new Utils();
// Function scroll

let lastKnownScrollPosition2 = 0;
let ticking2 = false;
var photoLeft = document.getElementById('photoLeft');
var target = document.getElementById('target3')
const photoRight = document.getElementById('photoRight');
var windowWidth = window.innerWidth;
function doSomethinge(scrollPos) {
    var isElementInView = Utils.isElementInView($('#target3'), false);
	var translateLeft = (100 - scrollPos/15);
	var translateRight = (-100 + scrollPos/15);
	var rotateLeft = (0 - scrollPos/1000);
	var rotateRight = (scrollPos/1000);
	var containerSmooth = (10 - scrollPos/100);
	if(windowWidth <= 480){
        var translateLeft = (100 - scrollPos/20);
		var translateRight = (-100 + scrollPos/20);
	}
	if (isElementInView) {
        if(translateLeft <= -10){
            const lastRotateLeft = photoLeft.style.transform
			photoLeft.style.transform = lastRotateLeft;
		}
		else{
            photoLeft.style.transform = ("translateX("+ translateLeft +"%) rotateZ("+ rotateLeft +"deg)");
		}
		if(translateRight >= 10){
			const lastRotateRight = photoRight.style.transform
			photoRight.style.transform = lastRotateRight;
		}
		else{
			photoRight.style.transform = ("translateX("+ translateRight +"%) rotateZ("+ rotateRight +"deg)");
		}
		target.style.transform = ("translate3d(0px,"+ containerSmooth +"%, 0px)");
	}
}

document.addEventListener("scroll", (event) => {
	lastKnownScrollPosition2 = window.scrollY;

	if (!ticking2) {
		window.requestAnimationFrame(() => {
			doSomethinge(lastKnownScrollPosition2);
			ticking2 = false;
		});

		ticking2 = true;
	}
});
document.addEventListener("resize", (e) => 	{
	lastKnownScrollPosition2 = window.scrollY;

	if (!ticking2) {
		window.requestAnimationFrame(() => {
			doSomethinge(lastKnownScrollPosition2);
			ticking2 = false;
		});

		ticking2 = true;
	}
});
