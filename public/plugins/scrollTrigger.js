
function isScrolledIntoView(elem)
{
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();

		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();

		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}
let flag = false;
function elementInViewport(el) {
    var myElement = el;
    var bounding = myElement.getBoundingClientRect();
    var myElementHeight = myElement.offsetHeight;
    var myElementWidth = myElement.offsetWidth;

    var bounding = myElement.getBoundingClientRect();

    if (bounding.top >= -myElementHeight
        && bounding.left >= -myElementWidth
        && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) + myElementWidth
        && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) + myElementHeight) {
        flag = true;
    } else {
        flag = false;
    }
}
function Utils() {

}

Utils.prototype = {
    constructor: Utils,
    isElementInView: function (element, fullyInView) {
            var pageTop = $(window).scrollTop();
            var pageBottom = pageTop + $(window).height();
            var elementTop = $(element).offsetTop;
            var elementBottom = elementTop + $(element).height();

            if (fullyInView === true) {
                    return ((pageTop < elementTop) && (pageBottom > elementBottom));
            } else {
                    return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
            }
    }
};

var Utils = new Utils();

var target = document.getElementById('floatImages')
var images = target.querySelectorAll('.float-img');
Array.prototype.forEach.call(images, function(el, index) {
    el.setAttribute("id","float" + index);
});
// Function scroll

let lastKnownScrollPosition = 0;
let ticking = false;
var image1 = document.getElementById('float0')
var image2 = document.getElementById('float1')
var windowWidth = screen.width;
function doSomething(scrollPos) {
    Array.prototype.forEach.call(images, function(el, index) {
        elementInViewport(target);
        if(windowWidth <= 600){
            if(index == 0){
                var translate = ((100 - scrollPos/10));
            }else{
                var translate = ((0 + scrollPos/10) - 100);
            }
            if (flag) {
                if(index == 0 && translate <= -100){
                    const lastPos = el.style.transform;
                    el.style.transform = lastPos;
                }else{
                    if(index == 1 && translate <= -200){
                        const lastPos = el.style.transform;
                        el.style.transform = lastPos;
                    }
                    else{
                        el.style.transform = ("translateY("+ translate +"%)");
                    }
                }
            }
        }else{
            if(index == 0){
                var translate = ((100 - scrollPos/10) + 480);
            }else{
                var translate = ((0 + scrollPos/25) - 230);
            }
            if (flag) {
                if(index == 0 && translate <= -50){
                    const lastPos = el.style.transform;
                    el.style.transform = lastPos;
                }else{
                    if(index == 1 && translate <= -100){
                        const lastPos = el.style.transform;
                        el.style.transform = lastPos;
                    }
                    else{
                        el.style.transform = ("translateY("+ translate +"%)");
                    }
                }
            }
        }


    });
}

document.addEventListener("scroll", (event) => {
	lastKnownScrollPosition = window.scrollY;

	if (!ticking) {
		window.requestAnimationFrame(() => {
			doSomething(lastKnownScrollPosition);
			ticking = false;
		});
		ticking = true;
	}
});
document.addEventListener("resize", (e) => 	{
	lastKnownScrollPosition = window.scrollY;

	if (!ticking) {
		window.requestAnimationFrame(() => {
			doSomething(lastKnownScrollPosition);
			ticking = false;
		});

		ticking = true;
	}
});
