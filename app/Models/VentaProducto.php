<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class VentaProducto extends Model
{
    use HasFactory;

    protected $table = 'venta_productos';
	protected $guarded = ['id'];


	public function venta()
    {
        return $this->belongsTo(Venta::class);
    }
	public function producto(){
		return $this->belongsTo(Producto::class, 'producto_id');
	}
}
