<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Producto;

use App\Models\VentaProducto;
use App\Models\Venta;
use PDF;


class AccesoController extends Controller
{

	public function dashboard()
	{
		return view('admin.dashboard');
	}
	public function pdf()
	{
		$ventas = Venta::all();
		$ventasProducto = VentaProducto::all();
		$pdf = PDF::loadView('admin.pdf', compact('ventas', 'ventasProducto'));
		return $pdf->download('reporte_ventas.pdf');
	}
	public function proveedor()
	{
		$alert = [
			'titulo' => 'Ups!',
			'mensaje' => 'El modulo aun esta en proceso',
			'estatus' => 'error'
		];

		return redirect()->back()->with('alert', $alert);
	}
}
