<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Image;

use App\Models\Venta;

class AlmacenController extends Controller
{

    public function index()
    {
        $ventas = Venta::all();
        return view('admin.almacen.index')->with('ventas', $ventas);
    }

}
