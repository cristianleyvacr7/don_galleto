<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Producto;
use App\Models\Venta;
use App\Models\VentaProducto;

use Illuminate\Http\Request;

class VentasController extends Controller
{
	public function vistaVenta(){
		$productos = Producto::all();
        return view('admin.venta.ventas')->with('productos', $productos);
	}
	public function realizarVenta(Request $request)
	{
		$formaVenta = $request->input('forma_venta');
		$ventaPrincipal = new Venta();
		$ventaPrincipal->forma_venta = $formaVenta;
		$precioTotalVenta = 0;
		$ventaPrincipal->fecha_compra = now();
		$productosSeleccionados = $request->input('productos_seleccionados');
		foreach ($productosSeleccionados as $productoId) {
			$producto = Producto::find($productoId);
			if (!$producto) {
				$data = [
					'titulo' => 'Error',
					'mensaje' => 'El producto con ID ' . $productoId . ' no existe.',
					'estatus' => 'error'
				];
				return redirect()->back()->with('alert', $data)->withInput();
			}

			$cantidadInputName = 'cantidad_' . $productoId;
			$cantidad = $request->input($cantidadInputName);

			if ($cantidad === null || !is_numeric($cantidad)) {
				continue;
			}
			if ($producto->stock < $cantidad) {
				$data = [
					'titulo' => 'Ups!',
					'mensaje' => 'No hay suficiente stock disponible para ' . $producto->nombre . '.',
					'estatus' => 'error'
				];
				return redirect()->back()->with('alert', $data)->withInput();
			}
			$precioTotal = $this->calcularPrecioTotal($formaVenta, $cantidad, $producto);
			if($formaVenta === 'unidad'){
				\Log::info('si entro a unidad');

				$producto->stock -= $cantidad;

			}
			if($formaVenta === 'caja'){
				\Log::info('si entro a caja');

				$producto->stock -= $cantidad;

			}
			if($formaVenta === 'agranel'){
				$cantidadDivision = $cantidad / 100;
				$producto->stock -= $cantidadDivision;
			}
			if($formaVenta === 'gramos'){
				$cantidadDivision = $cantidad / 100;
				$producto->stock -= $cantidadDivision;
			}
			$producto->save();

			$precioTotalVenta += $precioTotal;

			$ventaPrincipal->precio_total = $precioTotalVenta;
			$ventaPrincipal->save();

			$ventaProducto = new VentaProducto([
				'venta_id' => $ventaPrincipal->id,
				'producto_id' => $productoId,
				'cantidad' => $cantidad,
				'total' => $precioTotal,
			]);

			$ventaProducto->save();
		}

		$alert = [
			'titulo' => '¡Gracias!',
			'mensaje' => 'Ventas realizadas con éxito.',
			'estatus' => 'success'
		];

		return redirect()->back()->with('alert', $alert);
	}


	private function calcularPrecioTotal($formaVenta, $cantidad, $producto)
	{
		switch ($formaVenta) {
			case 'unidad':
				$precio = 10;
				return $cantidad * $precio;
			case 'caja':
				$precio = 100;
				return $cantidad * $precio;
			case 'gramos':
					$precio = 0.1;
				return $cantidad * $precio;
			case 'agranel':
				$precio = 0.1;
				return $cantidad * $precio;
			default:
				return 0;
		}
	}


}
