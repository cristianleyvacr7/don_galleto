<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Image;

use App\Models\Producto;

class ProductosController extends Controller
{

    public function index()
    {
        $producto = Producto::all();
        return view('admin.producto.index')->with('producto', $producto);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

			$productoExistente = Producto::where('nombre', $request->input('nombre'))->first();
			if ($productoExistente) {
				$data = array(
					'titulo' => 'Ups!',
					'mensaje' => 'Ya existe un producto con el mismo nombre. Por favor, elige otro nombre.',
					'estatus' => 'error'
				);
				return redirect()->back()->with('alert', $data)->withInput();
			}
			if ($request->input('precio_fabricacion') >= $request->input('precio_venta')) {
				$data = array(
					'titulo' => 'Ups!',
					'mensaje' => 'El precio de fabricación debe ser menor que el precio de venta.',
					'estatus' => 'error'
				);
				return redirect()->back()->with('alert', $data)->withInput();
			}
			if ($request->hasFile('imagen')) {
				$rutaImagen = self::save_img('', $request->file('imagen'), 1920, 1004);
			}
			$producto = new Producto();
            $producto->nombre = $request->input('nombre');
            $producto->precio_fabricacion = $request->input('precio_fabricacion');
            $producto->stock = $request->input('stock');
            $producto->precio_venta = $request->input('precio_venta');
			$producto->imagen = $rutaImagen;
            $producto->save();
            DB::commit();
			$alert = array(
				'titulo' => '¡Gracias!',
				'mensaje' => 'Se agregó el producto correctamente.',
				'estatus' => 'success'
			);
			return redirect()->back()->with('alert', $alert);
        }catch(\Exception $e){
            DB::rollBack();
            \Log::info($e);
            $mensaje = 'Ocurrió un error intenta nuevamente, si el problema persiste contacta al administrador.';
            $data = array(
                'estatus' => 'error',
                'mensaje' => $mensaje
            );
            return back()->with('alert', $data);
        }
    }
	public function agregarStock(Request $request) {
		$request->validate([
			'nombre' => 'required|exists:productos,id',
			'stock' => 'required|numeric|min:1',
		]);

		$producto = Producto::find($request->input('nombre'));
		$nuevoStock = $producto->stock + $request->input('stock');

		$producto->update(['stock' => $nuevoStock]);
		$alert = array(
			'titulo' => '¡Gracias!',
			'mensaje' => 'Se agregó mas stock al producto'. $producto->nombre,
			'estatus' => 'success'
		);
		return redirect()->back()->with('alert', $alert);
	}

    public function destroy($id)
    {
        DB::beginTransaction();
		try{
			$producto = Producto::find($id);
			$producto->delete();

            $pathFile = public_path().'/images/galletas';
            if(file_exists($pathFile.'/'.$producto->imagen))
            {
                unlink($pathFile.'/'.$producto->imagen);
            }
			$data = array(
				'estatus' => 'success',
				'mensaje' => "Eliminado correctamente",
				'id' => $id
			);
			DB::commit();
			return response()->json($data);
		} catch (\Exception $e) {
			DB::rollBack();
			$mensaje = 'Ocurrió un error al borrar el producto, intenta nuevamente, si el problema persiste contacta al administrador.';
			$data = array(
				'estatus' => 'error',
				'mensaje' => $mensaje,
				'error' => $e->getMessage(),
				'line' => $e->getLine(),
			);
		}
		return response()->json($data);
    }

    static function save_img($current, $image, $w, $h){
		$pathFile = public_path().'/images/galletas';
		if($current != '' && file_exists($pathFile.'/'.$current))
		{
			unlink($pathFile.'/'.$current);
		}

		$image_name = md5(time().rand(0,10)).$w.$h.".".$image->getClientOriginalExtension();
		$img = Image::make($image)
		->resize($w,null, function($constraint)
		{
			$constraint->aspectRatio();
		})
		->save('images/galletas/'.$image_name);

		return $image_name;
	}
}
