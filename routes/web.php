<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', "AccesoController@dashboard")->name('admin_dashboard');

//Inventarios
Route::get('/inventario', "ProductosController@index")->name('inventario');
Route::post('/guardarGalletas', "ProductosController@store")->name('guardarGalletas');
Route::post('/agregarStock', "ProductosController@agregarStock")->name('agregarStock');
Route::delete('/borrarGalletas/{id}', "ProductosController@destroy")->name('borrarGalletas');

//ventas
Route::get('/venta', "VentasController@vistaVenta")->name('ventas');
Route::post('/realizarVenta', 'VentasController@realizarVenta')->name('realizarVenta');

Route::get('/reporte', "AlmacenController@index")->name('reporte');

Route::get('/pdf', "AccesoController@pdf")->name('pdf');
Route::get('/proveedor', "AccesoController@proveedor")->name('proveedor');
