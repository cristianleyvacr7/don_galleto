const mix = require('laravel-mix');
mix.disableNotifications();

mix.browserSync({
    proxy:'don_galleto.test',
    port: 23085
});
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);
mix.stylus('resources/stylus/admin/login.styl', 'public/css/admin').version().options({ processCssUrls: false });
mix.stylus('resources/stylus/admin/admin.styl', 'public/css/admin').version().options({ processCssUrls: false });
mix.stylus('resources/stylus/ventas.styl', 'public/css').version().options({ processCssUrls: false });
