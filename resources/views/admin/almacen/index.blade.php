@extends('admin.layout')

@section('contenido')
    <div class="contenido" style="padding: 40px 30px 0">
        <div class="Panel-container Panel-container-view">
            <div class="Panel-container-view-actions" style="display: flex; justify-content: space-between;">
				<a href="/" class="btn btn--add">
					regresar
				</a>
				<div>
					<a href="{{ route('pdf') }}" id="agregar" class="btn btn--add" href="">
						Generar reporte mas epecifico en PDF
					</a>
				</div>
            </div>

            @if($ventas->count() > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Forma de venta</th>
                            <th>Fecha compra</th>
                            <th>Precio total</th>

                    </thead>
                    <tbody>
                        @foreach($ventas as $venta)
                            <tr id="row-{{$venta->id}}">
                                <td>{{ $venta->forma_venta}}</td>
                                <td>{{ $venta->fecha_compra}}</td>
                                <td>{{ $venta->precio_total}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p class="not-found">Aún no se han cargado las ventas al sistema</p>
            @endif
        </div>
    </div>

@stop

@section('js')
	<script type="text/javascript">
		$('#menu-banners').addClass('is-selected');
	</script>
@stop
