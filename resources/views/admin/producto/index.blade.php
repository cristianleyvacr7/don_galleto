@extends('admin.layout')

@section('contenido')
    <div class="contenido" style="padding: 40px 30px 0">
        <div class="Panel-container Panel-container-view">
            <div class="Panel-container-view-actions" style="display: flex; justify-content: space-between;">
				<a href="/" class="btn btn--add">
					regresar
				</a>
				<div>
					<button id="agregar" class="btn btn--add" href="">
						Agregar nueva galleta
					</button>
					<button id="stock" class="btn btn--add" href="">
						Agregar mas stock galleta
					</button>
				</div>
            </div>

            @if($producto->count() > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Imagen</th>
                            <th>Nombre</th>
                            <th>precio fabricacion</th>
                            <th>precio venta</th>
                            <th>stock</th>
							<th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($producto as $item)
                            <tr id="row-{{$item->id}}">
                                <td>
                                    <img style="max-width: 80px" src="{{asset('images/galletas/'.$item->imagen)}}" alt="">
                                </td>
                                <td>{{ $item->nombre}}</td>
                                <td>{{ $item->precio_fabricacion}}</td>
                                <td>{{ $item->precio_venta}}</td>
                                <td>{{ $item->stock}}</td>
                                <td class="Panel-actions">
                                    <button class="btn btn--delete-no-data" data-url="{{route('borrarGalletas',$item->id)}}">Eliminar</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p class="not-found">Aún no se han cargado los Prodcutos al sistema</p>
            @endif
        </div>
		<div class="panel-agregar">
			<div class="Panel">
				<h2>Agrega un galleta</h2>
				<div class="cerrar">
					<img src="images/tacha.png" alt="">
				</div>
				<form action="{{route('guardarGalletas')}}" method="POST" id="form" files="true" enctype="multipart/form-data">
					@csrf
					<div class="inputs">
						<div class="input text">
							<label for="name">Nombre</label>
							<input type="text" name="nombre" id="nombre">
						</div>
						<div class="input text">
							<label for="precio_fabricacion">Precio de fabricación</label>
							<input type="text" name="precio_fabricacion" id="precio_fabricacion">
						</div>
					</div>
					<div class="inputs">
						<div class="input text">
							<label for="stock">Stock</label>
							<input type="text" name="stock" id="stock">
						</div>
						<div class="input text">
							<label for="precio_venta">Precio venta</label>
							<input type="text" name="precio_venta" id="precio_venta">
						</div>
					</div>
					<div class="input text">
						<div class="input image">
							<p>Imagen</p>
							<div class="container">
								<i class="icon icon-close">x</i>
								<span class="btn btn--save">Seleccionar archivo (jpg, png)</span>
								<input type="file" data-img="imagen_1" name="imagen" id="data-imagen" class="input-file validate[required]"  accept="image/x-png,image/jpeg">
								<div class="imagen">
									<img id="img_imagen_1"  src="#"/>
								</div>
							</div>
						</div>
					</div>

					<div class="button">
						<button class="btn btn--save btn--large">Guardar</button>
					</div>
				</form>
			</div>
		</div>
		<div class="panel-stock">
			<div class="Panel">
				<h2>Agrega stock a galleta</h2>
				<div class="cerrar">
					<img src="images/tacha.png" alt="">
				</div>
				<form action="{{ route('agregarStock') }}" method="POST" id="form" files="true" enctype="multipart/form-data">
					@csrf
					<div class="inputs">
						<div class="input select">
							<label for="name">Elige la galleta</label>
							<select name="nombre">
									<option value="">Selecciona una galleta</option>
								@foreach ($producto as $item)
									<option value="{{ $item->id }}">{{ $item->nombre }}</option>
								@endforeach
							</select>
						</div>
						<div class="input text">
							<label for="stock">Stock</label>
							<input type="text" name="stock" id="stock">
						</div>
					</div>
					<div class="button">
						<button class="btn btn--save btn--large">Guardar</button>
					</div>
				</form>
			</div>
		</div>
    </div>

@stop

@section('js')
	<script type="text/javascript">
		$('#menu-banners').addClass('is-selected');
		$('#agregar').on('click', function(){
			$('.panel-agregar').addClass('selected');
		})
		$('#stock').on('click', function(){
			$('.panel-stock').addClass('selected');
		})
		$('.cerrar').on('click', function(){
			$('.panel-agregar').removeClass('selected');
			$('.panel-stock').removeClass('selected');
		})

	</script>
@stop
