 @extends('admin.layout')


@section('contenido')

	<div class="Panel-titulo">
		<h1>Editar Banner</h1>
	</div>
	<div class="Panel">
		<form action="{{route('banner.update',$banner->id)}}" method="POST" id="form" files="true" enctype="multipart/form-data">
			@method('PUT')
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="inputs">
				<div class="input text">
					<div class="input image">
						<p>*Imagen(1920x1080)</p>
						<div class="container">
							<i class="icon icon-close">x</i>
							<span class="btn btn--save">Seleccionar archivo (jpg, png)</span>
							<input type="file" data-img="imagen_1" name="imagen" id="data-imagen" class="input-file"  accept="image/x-png,image/jpeg">
							<div class="imagen">
								<img id="img_imagen_1" class="is-visible" src="{{asset('/images/banner/'.$banner->imagen)}}"/>
							</div>
						</div>
					</div>
				</div>
				<div class="input text">
					<div class="input image">
						<p>*Imagen movil(1080x1920)</p>
						<div class="container">
							<i class="icon icon-close">x</i>
							<span class="btn btn--save">Seleccionar archivo (jpg, png)</span>
							<input type="file" data-img="imagen_movil" name="imagen_movil" id="data-imagen_movil" class="input-file"  accept="image/x-png,image/jpeg">
							<div class="imagen">
								<img id="img_imagen_movil" class="is-visible" src="{{asset('/images/banner/'.$banner->imagen_movil)}}"/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="inputs">
				<div class="input text">
					<label for="name">Link</label>
					<input type="text" name="link" id="link" value="{{$banner->link}}">
				</div>

				<div class="input input-radioGroup input-50 input-l">
					<p>*¿Publicado?</p>
					<label class="input-radio">
						<input type="radio" id="cbox" value="1" name="publicado" {{$banner->publicado == 1 ? 'checked' : ''}}>
						<span class="outer">
							<span class="inner"></span>
						</span> Sí
					</label>
					<label class="input-radio">
						<input type="radio" id="cbox" value="0" name="publicado" {{$banner->publicado == 0 ? 'checked' : ''}}>
						<span class="outer">
							<span class="inner"></span>
						</span> No
					</label>
				</div>

			</div>
			<div class="button">
				<button class="btn btn--save btn--large">Guardar</button>
			</div>
		</form>
	</div>
@stop

@section('js')
	<script type="text/javascript">
		$(function(){
			$('#menu-banners').addClass('is-selected');
		});
	</script>
@stop
