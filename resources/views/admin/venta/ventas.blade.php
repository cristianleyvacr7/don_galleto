@extends('admin.layout')
@section('head')
	<title>VENTAS</title>
    <link rel="stylesheet" href="{{mix('css/ventas.css')}}">
@endsection
@section('contenido')
    <div class="ventas">
        <div class="ventas-titulo">
            <h1>Modulo venta</h1>
        </div>
		<div class="Panel-container-view-actions" style="display: flex; justify-content: space-between;padding-left: 40px">
			<a href="/" class="btn btn--add">
				regresar
			</a>
		</div>
        <form action="{{route('realizarVenta')}}" method="POST" id="form" class="ventas-contenido">
			@csrf
            <div class="ventas-contenido-opciones">
				<input type="hidden" name="forma_venta" id="forma_venta_input" value="">
                <div class="ventas-contenido-opciones-opcion" data-forma="unidad">
                    <div class="img">
                        <img src="{{ asset('/images/icon_peso.png') }}" alt="">
                    </div>
                    <p>Unidad</p>
                </div>
                <div class="ventas-contenido-opciones-opcion" data-forma="gramos">
                    <div class="img">
                        <img src="{{ asset('/images/peso.png') }}" alt="">
                    </div>
                    <p>Peso por gramos</p>
                </div>
                <div class="ventas-contenido-opciones-opcion" data-forma="agranel">
                    <div class="img">
                        <img src="{{ asset('/images/icon_dinero.png') }}" alt="">
                    </div>
                    <p>Peso por dinero</p>
                </div>
                <div class="ventas-contenido-opciones-opcion" data-forma="caja">
                    <div class="img">
                        <img src="{{ asset('/images/icon_caja.png') }}" alt="">
                    </div>
                    <p>Caja</p>
                </div>
            </div>

			<div class="unidad">
				@foreach($productos as $producto)
					<div class="unidad-opcion">
						<div class="unidad-opcion-imagen">
							<div class="input">
								<input class="checkbox" type="checkbox" name="productos_seleccionados[]" value="{{ $producto->id }}">
							</div>
							<div class="img">
								<img src="{{ asset('/images/galletas/'.$producto->imagen ) }}" alt="">
							</div>
						</div>
						<div class="input">
							<input type="text" class="cantidad-input" name="cantidad_{{ $producto->id }}" placeholder="Cant. para {{ $producto->nombre }}" >
						</div>
					</div>
				@endforeach
			</div>
			<br>
			<div class="input text">
				<label for="">Precio total</label>
				<input type="text" name="precio_total" id="precio-total">
			</div>
			<br>
			<div class="boton">
				<button class="btn btn--add">guardar</button>
			</div>
			<br><br>
        </form>
    </div>
@endsection

@section('js')

<script>
    $(document).ready(function() {
        // Escuchar cambios en los inputs de cantidad
        $('.cantidad-input').on('change', function() {
            calcularPrecioTotal();
        });

        // Escuchar cambios en las opciones de forma de venta
        $('.ventas-contenido-opciones-opcion').on('click', function() {
            var formaVenta = $(this).data('forma');
            $('#forma_venta_input').val(formaVenta);
            calcularPrecioTotal();
        });

        function calcularPrecioTotal() {
            var formaVenta = $('#forma_venta_input').val();
            var suma = 0;
            $('.cantidad-input').each(function() {
                var cantidad = parseFloat($(this).val()) || 0;
                switch (formaVenta) {
                    case 'unidad':
                        suma += cantidad * 10;
                        break;
                    case 'gramos':
                        suma += (cantidad / 100) * 10;
                        break;
						case 'agranel':
							suma +=  (cantidad / 100) * 10;
                        break;
                    case 'caja':
                        suma += cantidad * 100;
                        break;
                }
            });

            // Actualizar el valor del input de precio total
            $('#precio-total').val(suma);
        }
    });
</script>
<script>
	$(document).ready(function() {
		$('.ventas-contenido-opciones-opcion').click(function() {
			$('.ventas-contenido-opciones-opcion .img').removeClass('selected');
			$(this).find('.img').addClass('selected');
			var formaVenta = $(this).data('forma');
			$('#campo_precio_agranel').hide();
			if (formaVenta === 'agranel') {
				$('#campo_precio_agranel').show();
			}
		});
	});
</script>
{{-- <script>
	$(document).ready(function(){
		var precioTotal = document.;
	})
</script> --}}
<script>
	 $(document).ready(function () {
        $(".ventas-contenido-opciones-opcion").click(function () {
            var formaSeleccionada = $(this).data("forma");
            $("#forma_venta_input").val(formaSeleccionada);
        });
    });
</script>
@endsection

