<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="noindex"/>
		<meta id="token" name="csrf-token" content="{{csrf_token()}}">
		<title>Don galleto</title>
		<script src="{{asset('/plugins/sweetalert/sweetalert.min.js')}}"></script>
		<link rel="stylesheet" type="text/css" href="{{asset('/plugins/sweetalert/sweetalert.css')}}">
		<link rel="stylesheet" href="{{ asset('/plugins/validationEngine/validationEngine.jquery.css') }}">
		<link rel="stylesheet" href="{{ asset('/plugins/dataTables/jquery.dataTables.min.css') }}">
		<link rel="stylesheet" href="{{ asset('/plugins/fancybox/jquery.fancybox.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/admin/admin.css') }}">
		<link rel="stylesheet" href="{{ asset('/css/admin.css') }}">

		@yield('head')
	</head>
	<body>
		<div class="spinner">
			<div id="loading"></div>
		</div>
        <div class="logo">
            <img src="images/logo.png" alt="">
        </div>
        <div class="icon-noti">
            <img src="images/noti.png" alt="">
        </div>
        <div class="notificacion">
            <div class="notificacion-contenido">
                <div class="icon-cerrar">
                    <img src="images/cerrar.png" alt="">
                </div>
                <div class="notificacion-contenido-noti">
                    <p>faltan galletas de chocolate</p>
                </div>
                <div class="notificacion-contenido-noti">
                    <p>Mañana viene un proveedor</p>
                </div>
                <div class="notificacion-contenido-noti">
                    <p>Revisa el almacen faltan algunas cosas</p>
                </div>
            </div>
        </div>
		@yield('contenido')

		<script src="{{ asset('plugins/jquery.min.js') }}"></script>
		<script src="{{ asset('/plugins/validationEngine/jquery.validationEngine.js') }}"></script>
		<script src="{{ asset('/plugins/validationEngine/jquery.validationEngine-es.js') }}"></script>
		<script src="{{ asset('/plugins/spin/spin.min.js') }}"></script>
		<script src="{{ asset('/plugins/dataTables/jquery.dataTables.min.js') }}"></script>
		<script src="{{ asset('/plugins/trumbowyg/trumbowyg.min.js') }}"></script>
		<script src="{{ asset('/plugins/trumbowyg/langs/es.min.js') }}"></script>

		@yield('js')
		<script>
			$(function(){
				@if(Session::get('alert') != null)
					swal("", "{{Session::get('alert')['mensaje']}}", "{{Session::get('alert')['estatus']}}");
				@endif

				$("div.input input.input-file").change(function(){
					loadImg(this);
				});

                function loadImg(input) {
					var $img = $(input).data('img');
					if (input.files && input.files[0]) {
						var reader = new FileReader();

						reader.onload = function (e) {
							$('#img_'+$img).attr('src', e.target.result);
						};

						reader.readAsDataURL(input.files[0]);
						$('#img_'+$img).css('display', 'block');
					}

				}

			    $('textarea.texteditor').trumbowyg({
					lang: 'es',
					btns: [
						['undo', 'redo'],
						['formatting'],
						['strong', 'em', 'del'],
						['link'],
						['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
						['unorderedList', 'orderedList'],
						['horizontalRule'],
						['removeformat'],
						['table']
					],
					autogrow: true,
					removeformatPasted: true
				});

				$('div.Panel-container-view table.table').DataTable({
					'language' : {
						"sProcessing":     "Procesando...",
						"sLengthMenu":     "Mostrar _MENU_ registros",
						"sZeroRecords":    "No se encontraron resultados",
						"sEmptyTable":     "Ningún dato disponible en esta tabla",
						"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
						"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
						"sInfoPostFix":    "",
						"sSearch":         "Buscar:",
						"sUrl":            "",
						"sInfoThousands":  ",",
						"sLoadingRecords": "Cargando...",
						"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
						},
						"oAria": {
							"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
							"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
					},
					'order': []
				});

				$.ajaxSetup({
					headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}
				});

				$('#form').validationEngine({
					scroll : false
				});

				$('div.Panel-container-view').on('click', 'button.btn--delete', function(){

					var $this = $(this);
					var $id = $this.data('id');
					var $url = $this.data('url');

					swal({
						title: "¿Estás seguro?",
						text: "¿Quiéres eliminar este elemento?",
						type: "warning",
						showCancelButton: true,
						cancelButtonText : 'Cancelar',
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Sí",
						closeOnConfirm: true
					}, function(){
						$('.spinner').show();
						$.post($url, { id : $id }, function(response){
							console.log(response);
							$('.spinner').hide();
							swal('', response.mensaje, response.estatus);
							if(response.estatus == 'success')
							{
								$('#row-'+$id).slideUp(400, function(){
									$(this).delay(400).remove();
								});
							}

						});
					});
				});

				$('div.Panel-container').on('click', 'button.btn--delete-no-data', function(){

					var $this = $(this);
					var $url = $this.data('url');
					var tipo_accion = $this.data('tipo');


					var tipo = 'eliminar';
					var texto = '';
					if(tipo_accion == 'cancelar'){
						tipo = 'cancelar';
						texto = 'Quieres cancelar esta venta'
					}

					swal({
					title: "¿Estás seguro?",
					text: tipo == 'eliminar' ? 'Quieres eliminar este elemento':texto,
					type: "warning",
					showCancelButton: true,
					cancelButtonText : 'Cancelar',
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Sí",
					closeOnConfirm: false
					},function(){
						$('.spinner').show();
						$.ajax({
							url : $url,
							type: 'POST',
							data: {_method: 'DELETE'},
							success: function(response) {
								$('.spinner').hide();
								if(tipo == 'eliminar'){
									swal('', response.mensaje, response.estatus);
								}
								if(response.estatus == 'success')
								{
									if(tipo == 'cancelar'){
										swal({
											title: "",
											text: response.mensaje,
											type: response.estatus
										}, function() {

											window.location.reload();
										});
									}
									if(tipo == 'eliminar'){
										$('#row-'+response.id).slideUp(400, function(){
											$(this).delay(400).remove();
										});
									}
								}
							}
						});
					});
				});

				var opts = {
					lines: 8 // The number of lines to draw
					, length: 18 // The length of each line
					, width: 5 // The line thickness
					, radius: 20 // The radius of the inner circle
					, scale: 1 // Scales overall size of the spinner
					, corners: 1 // Corner roundness (0..1)
					, color: '#FFFFFF' // #rgb or #rrggbb or array of colors
					, opacity: 0.25 // Opacity of the lines
					, rotate: 0 // The rotation offset
					, direction: 1 // 1: clockwise, -1: counterclockwise
					, speed: 1 // Rounds per second
					, trail: 60 // Afterglow percentage
					, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
					, zIndex: 2e9 // The z-index (defaults to 2000000000)
					, className: 'spinner' // The CSS class to assign to the spinner
					, top: '50%' // Top position relative to parent
					, left: '50%' // Left position relative to parent
					, shadow: true // Whether to render a shadow
					, hwaccel: false // Whether to use hardware acceleration
					, position: 'absolute' // Element positioning
				};

				var target = document.getElementById('loading');
				var spinner = new Spinner(opts).spin(target);

			});
		</script>

	</body>
</html>
