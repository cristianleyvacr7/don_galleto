<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<style>
		table {
			width: 100%;
			border-collapse: collapse;
			margin-top: 20px;
		}
		th, td {
			border: 1px solid #ddd;
			padding: 8px;
			text-align: center;
		}
		th {
			background-color: #f2f2f2;
		}
		title{
			margin: 0
		}
	</style>
</head>
<body>
	<div class="conenido" style="text-align: center">
		<img style="margin: 0 auto" src="images/logo.png" alt="">
	</div>
	<table>
		<thead>
			<tr>
				<th>Numero de venta</th>
				<th>Forma de venta</th>
				<th>Fecha compra</th>
				<th>Precio total</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ventas as $venta)
				<tr id="row-{{$venta->id}}">
					<td>{{ $venta->id}}</td>
					<td>{{ $venta->forma_venta}}</td>
					<td>{{ $venta->fecha_compra}}</td>
					<td>{{ $venta->precio_total}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<br><br>
	<table class="table">
		<thead>
			<tr>
				<th>Numero de venta</th>
				<th>Nombre del producto</th>
				<th>Cantidad de galletas</th>
				<th>Total de dinero</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ventasProducto as $venta)
				<tr id="row-{{$venta->id}}">
					<td>{{ $venta->venta_id}}</td>
					<td>{{ $venta->producto ? $venta->producto->nombre : 'Producto no encontrado' }}</td>
					<td>{{ $venta->cantidad}}</td>
					<td>{{ $venta->total}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
