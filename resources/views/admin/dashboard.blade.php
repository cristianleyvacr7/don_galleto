@extends('admin.layout')


@section('contenido')

    <div class="inicio">
        <div class="inicio-opciones">
            <a href="{{ route('inventario') }}" class="inicio-opciones-opcion">
                <img src="images/image.png" alt="">
                <p>INVENTARIO</p>
            </a>
            <a href="{{ route('ventas') }}" class="inicio-opciones-opcion">
                <img src="images/image2.png" alt="">
                <p>VENTA</p>
            </a>
            <a href="{{ route('reporte') }}" class="inicio-opciones-opcion">
                <img src="images/reporte.png" alt="">
                <p>REPORTE </p>
            </a>
            <a href="{{ route('proveedor') }}" class="inicio-opciones-opcion">
                <img src="images/image4.png" alt="">
                <p>PROVEEDOR</p>
            </a>
        </div>
    </div>

@stop
